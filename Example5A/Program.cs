﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example5A
{
    class Program
    {
        const string ExchangeName = "ExchangeExample5";
        const string QueueName = "QueueExample5";
        static void Main(string[] args)
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();
                ModelCentralized.QueueDeclare(QueueName, false, false, true, null);
                ModelCentralized.ExchangeDeclare(ExchangeName, ExchangeType.Direct, false);
                ModelCentralized.QueueBind(QueueName, ExchangeName, "");
                ModelCentralized.BasicQos(0, 1, false);

                QueueingBasicConsumer consumer = new QueueingBasicConsumer(ModelCentralized);
                string consumerTag = ModelCentralized.BasicConsume(QueueName, false, consumer);

                Console.WriteLine("Wait incoming message...");

                while (true)
                {
                    var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    IBasicProperties props = e.BasicProperties;
                    string replyQueue = props.ReplyTo;
                    string messageId = props.MessageId;

                    string content = Encoding.Default.GetString(e.Body);
                    Console.WriteLine("> {0}", content);

                    int result = GetSum(content);
                    Console.WriteLine("< {0}", result);
                    var msgRaw = Encoding.Default.GetBytes(result.ToString());
                    IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();
                    basicProperties.MessageId = messageId;
                    ModelCentralized.BasicPublish("", replyQueue, basicProperties, msgRaw);

                    ModelCentralized.BasicAck(e.DeliveryTag, false);
                }
            }
        }

        private static int GetSum(string content)
        {
            var numbers = content.Split("+".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (numbers.Length != 2)
            {
                return -1;
            }

            try
            {
                return int.Parse(numbers[0]) + int.Parse(numbers[1]);
            }
            catch
            {
                return -1;
            }
        }
    }
}
