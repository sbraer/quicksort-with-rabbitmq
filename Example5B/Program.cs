﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Example5B
{
    class Program
    {
        const string ExchangeName = "ExchangeExample5";
        static Dictionary<string, string> messageBuffers;
        static string QueueName;
        static void Main(string[] args)
        {
            messageBuffers = new Dictionary<string, string>();
            messageBuffers.Add("a1", "2+2");
            messageBuffers.Add("a2", "3+3");
            messageBuffers.Add("a3", "4+4");

            QueueName = Guid.NewGuid().ToString();

            var t1 = new Thread(new ThreadStart(SendMessage));
            var t2 = new Thread(new ThreadStart(ReceiveMessage));
            t2.Start();
            t1.Start();
            t1.Join();
            t2.Join();
            Console.ReadLine();
        }

        static void SendMessage()
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";

            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();

                Console.WriteLine("Send messages...");
                byte[] msgRaw;

                foreach (KeyValuePair<string, string> item in messageBuffers)
                {
                    IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();
                    basicProperties.MessageId = item.Key; // a1, a2, a3
                    basicProperties.ReplyTo = QueueName;
                    msgRaw = Encoding.Default.GetBytes(item.Value); // 2+2, 3+3, 4+4
                    ModelCentralized.BasicPublish(ExchangeName, "", basicProperties, msgRaw);
                }
            }
        }

        static void ReceiveMessage()
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();
                ModelCentralized.QueueDeclare(QueueName, false, true, true, null);
                ModelCentralized.BasicQos(0, 1, false);

                QueueingBasicConsumer consumer = new QueueingBasicConsumer(ModelCentralized);
                string consumerTag = ModelCentralized.BasicConsume(QueueName, false, consumer);

                Console.WriteLine("Wait incoming message...");

                while (true)
                {
                    var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    string content = Encoding.Default.GetString(e.Body);
                    string messageId = e.BasicProperties.MessageId;
                    Console.WriteLine("{0} = {1}", messageBuffers[messageId] , content);
                    ModelCentralized.BasicAck(e.DeliveryTag, false);
                }
            }
        }
    }
}
