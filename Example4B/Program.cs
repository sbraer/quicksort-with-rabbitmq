﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example4B
{
    class Program
    {
        const string ExchangeName = "ExchangeExample4";
        const string RoutingKey = "RoutingExample4";
        static void Main(string[] args)
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();

                Console.WriteLine("Send messages...");
                IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();
                int i = 0;
                byte[] msgRaw;

                while (true)
                {
                    for (int t = 0; t < 100; t++, i++)
                    {
                        msgRaw = Encoding.Default.GetBytes(i.ToString());
                        ModelCentralized.BasicPublish(ExchangeName, RoutingKey, basicProperties, msgRaw);
                    }

                    if (Console.ReadLine().Length == 0)
                    {
                        break;
                    }
                }
            }
        }
    }
}

