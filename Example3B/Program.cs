﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example3B
{
    class Program
    {
        const string ExchangeName = "ExchangeExample3";
        static void Main(string[] args)
        {
            var messages = new string[] {
                "small.red","big.red","big.white", "small.white"
            };
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();

                Console.WriteLine("Send messages...");
                IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();

                byte[] msgRaw;
                foreach (var message in messages)
                {
                    msgRaw = Encoding.Default.GetBytes(message);
                    ModelCentralized.BasicPublish(ExchangeName, message, basicProperties, msgRaw);
                }
            }

            Console.Write("Enter to exit... ");
            Console.ReadLine();
        }
    }
}
