﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example1A
{
    class Program
    {
        const string QueueName = "Example1";
        static void Main(string[] args)
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";

            /*connectionFactory.HostName = "chicken.rmq.cloudamqp.com";
            connectionFactory.UserName = "xxx";
            connectionFactory.Password = "yyyy";
            connectionFactory.VirtualHost = "wwww";*/

            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();
                ModelCentralized.QueueDeclare(QueueName, false, true, false, null);

                QueueingBasicConsumer consumer = new QueueingBasicConsumer(ModelCentralized);
                string consumerTag = ModelCentralized.BasicConsume(QueueName, true, consumer);

                Console.WriteLine("Wait incoming message...");

                while (true)
                {
                    var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    string content = Encoding.Default.GetString(e.Body);
                    Console.WriteLine("> {0}", content);
                    if (content == "10")
                    {
                        break;
                    }
                }
            }

            Console.Write("Enter to exit... ");
            Console.ReadLine();
        }
    }
}
