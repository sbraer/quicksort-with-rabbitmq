﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Text;

namespace Example2A
{
    class Program
    {
        const string ExchangeName = "ExchangeExample2";
        static void Main(string[] args)
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();
                string QueueName = ModelCentralized.QueueDeclare("", false, true, true, null);
                ModelCentralized.ExchangeDeclare(ExchangeName, ExchangeType.Fanout);
                ModelCentralized.QueueBind(QueueName, ExchangeName, "");

                QueueingBasicConsumer consumer = new QueueingBasicConsumer(ModelCentralized);
                string consumerTag = ModelCentralized.BasicConsume(QueueName, false, consumer);

                Console.WriteLine("Wait incoming message...");

                while (true)
                {
                    var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    string content = Encoding.Default.GetString(e.Body);
                    Console.WriteLine("> {0}", content);
                    ModelCentralized.BasicAck(e.DeliveryTag, false);
                    if (content == "10")
                    {
                        break;
                    }
                }
            }

            Console.Write("Enter to exit... ");
            Console.ReadLine();
        }
    }
}
