﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example1B
{
    class Program
    {
        const string QueueName = "Example1";
        static void Main(string[] args)
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();

                Console.WriteLine("Send messages...");
                IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();

                byte[] msgRaw;
                for (int i = 0; i < 11; i++)
                {
                    msgRaw = Encoding.Default.GetBytes(i.ToString());
                    ModelCentralized.BasicPublish("", QueueName, basicProperties, msgRaw);
                }
            }

            Console.Write("Enter to exit... ");
            Console.ReadLine();
        }
    }
}
