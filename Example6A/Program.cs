﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Example6A
{
    class Program
    {
        const string QueueName = "Example6";
        static void Main(string[] args)
        {
            var content = new int[] { 1, 2, 3, 4 };

            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();

                Console.WriteLine("Send messages...");
                IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();
                var msgRaw = ObjectToByteArray<int[]>(content);
                ModelCentralized.BasicPublish("", QueueName, basicProperties, msgRaw);

                var json = new JavaScriptSerializer().Serialize(content);
                msgRaw = Encoding.Default.GetBytes(json);
                ModelCentralized.BasicPublish("", QueueName, basicProperties, msgRaw);
            }


            Console.Write("Enter to exit... ");
            Console.ReadLine();
        }

        public static byte[] ObjectToByteArray<T>(T obj) where T : class
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

    }
}
