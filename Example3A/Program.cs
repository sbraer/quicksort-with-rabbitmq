﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Example3A
{
    class Program
    {
        const string ExchangeName = "ExchangeExample3";
        static void Main(string[] args)
        {
            var receiver1 = new ReceiveClass("Process1", ExchangeName, "*.red");
            var receiver2 = new ReceiveClass("Process2", ExchangeName, "small.*");
            var thread1 = new Thread(new ThreadStart(receiver1.ReceiveMessage));
            var thread2 = new Thread(new ThreadStart(receiver2.ReceiveMessage));

            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();
        }
    }

    public class ReceiveClass
    {
        private readonly string _name, _routingKey, _exchangeName;
        private ReceiveClass() { }
        public ReceiveClass(string name, string exchangeName, string routingKey)
        {
            _name = name;
            _exchangeName = exchangeName;
            _routingKey = routingKey;
        }
        public void ReceiveMessage()
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();
                string QueueName = ModelCentralized.QueueDeclare("", false, true, true, null);
                ModelCentralized.ExchangeDeclare(_exchangeName, ExchangeType.Topic);
                ModelCentralized.QueueBind(QueueName, _exchangeName, _routingKey);

                QueueingBasicConsumer consumer = new QueueingBasicConsumer(ModelCentralized);
                string consumerTag = ModelCentralized.BasicConsume(QueueName, false, consumer);

                Console.WriteLine("Wait incoming message...");

                while (true)
                {
                    var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    string content = Encoding.Default.GetString(e.Body);
                    Console.WriteLine("[{0}]> {1}", _name, content);
                    ModelCentralized.BasicAck(e.DeliveryTag, false);
                    if (content == "10")
                    {
                        break;
                    }
                }
            }
        }
    }
}
