﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuickSortAsync
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            var arrayToSort = new List<int>();

            for (int i = 0; i < 50; i++)
            {
                arrayToSort.Add(rnd.Next(1, 51));
            }

            // show before sort
            Console.Write("Actual order: ");
            arrayToSort.ForEach(t => Console.Write(" {0}", t));
            Console.WriteLine();

            // Sort sync
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            var arraySortedSync = QuickSortSync(arrayToSort);
            stopwatch.Stop();
            Console.WriteLine("Time elapsed: {0}", stopwatch.Elapsed);
            Console.Write("Ordered with sync: ");
            arraySortedSync.ForEach(t => Console.Write(" {0}", t));
            Console.WriteLine();
        }

        static List<int> QuickSortSync(List<int> toOrder)
        {
            if (toOrder.Count <= 1)
            {
                return toOrder;
            }

            int pivot_index = toOrder.Count / 2;
            int pivot_value = toOrder[pivot_index];

            var less = new List<int>();
            var greater = new List<int>();

            for (int i = 0; i < toOrder.Count; i++)
            {
                if (i == pivot_index)
                {
                    continue;
                }

                if (toOrder[i] < pivot_value)
                {
                    less.Add(toOrder[i]);
                }
                else
                {
                    greater.Add(toOrder[i]);
                }
            }

            var lessOrdered = QuickSortSync(less);
            var greaterOrdered = QuickSortSync(greater);

            var result = new List<int>();
            result.AddRange(lessOrdered);
            result.Add(pivot_value);
            result.AddRange(greaterOrdered);

            return result;
        }
    }
}
