﻿using RabbitMQ.Client;
using System;
using System.Text;

namespace Example2B
{
    class Program
    {
        const string ExchangeName = "ExchangeExample2";
        static void Main(string[] args)
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();

                Console.WriteLine("Send messages...");
                IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();

                byte[] msgRaw;
                for (int i = 0; i < 11; i++)
                {
                    msgRaw = Encoding.Default.GetBytes(i.ToString());
                    ModelCentralized.BasicPublish(ExchangeName, "", basicProperties, msgRaw);
                }
            }

            Console.Write("Enter to exit... ");
            Console.ReadLine();

        }
    }
}
