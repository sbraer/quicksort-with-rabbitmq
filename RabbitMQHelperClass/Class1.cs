﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQLibrary
{
    public class RabbitHelper : IDisposable
    {
        private string _connectionStringRabbit, _publicQueueName, _exchangeName, _routingKey;
        private RabbitHelper() { }
        public RabbitHelper(string connectionStringRabbit, string publicQueueName) : this(connectionStringRabbit, publicQueueName, Guid.NewGuid().ToString(), Guid.NewGuid().ToString())
        { }

        public RabbitHelper(string connectionStringRabbit, string publicQueueName, string exchangeName, string routingKey)
        {
            _connectionStringRabbit = connectionStringRabbit;
            _publicQueueName = publicQueueName;
            _exchangeName = exchangeName;
            _routingKey = routingKey;
        }

        public void Dispose()
        {
        }
    }
}
