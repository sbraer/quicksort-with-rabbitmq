﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RabbitMQLibrary
{
    public partial class RabbitHelper : IDisposable
    {
        private string _connectionStringRabbit;
        private string QueuePublicName, ExchangePublicName, RoutingKeyPublicName;

        private ConnectionFactory connectionFactory;
        private IConnection Connection;
        private IModel ModelCentralized;
        private List<PrivateMessageQueue> privateThreadCollection;
        private Thread queuePublicThread;

        public delegate void ReceivedPublicMessage(object sender, RabbitMQEventArgs e);
        public event ReceivedPublicMessage OnReceivedPublicMessage;

        private RabbitHelper() { }
        public RabbitHelper(string connectionStringRabbit)
        {
            _connectionStringRabbit = connectionStringRabbit;
            privateThreadCollection = new List<PrivateMessageQueue>();
        }

        public void SendMessageToPublicQueue(byte[] msgRaw, string messageId, string queueReply)
        {
            if (connectionFactory == null)
            {
                connectionFactory = new ConnectionFactory();
            }

            // Fire & Forget
            Task.Run(() =>
            {
                IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();
                basicProperties.MessageId = messageId;
                basicProperties.ReplyTo = queueReply;
                //basicProperties.Headers = new Dictionary<string, object>();
                //basicProperties.Headers.Add("MessageId", messageId);
                //basicProperties.Headers.Add("QueueName", queueReply);

                ModelCentralized.BasicPublish(ExchangePublicName, RoutingKeyPublicName, basicProperties, msgRaw);
            });
        }

        public void SendMessageToPrivateQueue(byte[] msgRaw, string messageId, string queueReply)
        {
            if (connectionFactory == null)
            {
                connectionFactory = new ConnectionFactory();
            }

            // Fire & Forget
            Task.Run(() =>
            {
                IBasicProperties basicProperties = ModelCentralized.CreateBasicProperties();
                basicProperties.MessageId = messageId;
                basicProperties.ReplyTo = queueReply;
                //basicProperties.Headers = new Dictionary<string, object>();
                //basicProperties.Headers.Add("MessageId", messageId);
                //basicProperties.Headers.Add("QueueName", queueReply);

                ModelCentralized.BasicPublish(string.Empty, queueReply, basicProperties, msgRaw);
            });
        }

        public PrivateMessageQueue AddPrivateQueue(string queueName = null)
        {
            PrivateMessageQueue pmq = new PrivateMessageQueue(Connection, connectionFactory, queueName);
            privateThreadCollection.Add(pmq);
            return pmq;
        }

        public void AddPublicQueue(string queueName, string exchangeName, string routingKey, bool durable = false)
        {
            if (ModelCentralized != null)
            {
                // Already created
                throw new ApplicationException("Public queue already open");
            }

            if (connectionFactory == null)
            {
                connectionFactory = new ConnectionFactory();
            }

            QueuePublicName = queueName;
            ExchangePublicName = exchangeName;
            RoutingKeyPublicName = routingKey;

            connectionFactory.HostName = _connectionStringRabbit;
            Connection = connectionFactory.CreateConnection();
            ModelCentralized = Connection.CreateModel();
            ModelCentralized.QueueDeclare(queueName, true, false, false, null);
            ModelCentralized.ExchangeDeclare(exchangeName, "direct");
            ModelCentralized.QueueBind(queueName, exchangeName, routingKey);

            queuePublicThread = new Thread(new ThreadStart(WaitQueuePublicRequest));
            queuePublicThread.IsBackground = true;
            queuePublicThread.Start();
        }

        private void WaitQueuePublicRequest()
        {
            QueueingBasicConsumer consumer;
            string consumerTag;
            lock (Connection)
            {
                consumer = new QueueingBasicConsumer(ModelCentralized);
                consumerTag = ModelCentralized.BasicConsume(QueuePublicName, false, consumer);
            }

            while (true)
            {
                try
                {
                    var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    IBasicProperties props = e.BasicProperties;
                    string messageId = props.MessageId;
                    string queueSender = props.ReplyTo;
                    //if (props.Headers["MessageId"] != null) { messageId = GetStringFromBytes((byte[])props.Headers["MessageId"]); }
                    //if (props.Headers["QueueName"] != null) { queueSender = GetStringFromBytes((byte[])props.Headers["QueueName"]); }

                    if (string.IsNullOrEmpty(messageId))
                    {
                        messageId = Guid.NewGuid().ToString();
                    }

                    byte[] body = e.Body;
                    if (OnReceivedPublicMessage != null)
                    {
                        var rabbitEventArgs = new RabbitMQEventArgs(messageId, queueSender, body);
                        if (OnReceivedPublicMessage != null)
                        {
                            // Fire & forget
                            Task.Run(() => OnReceivedPublicMessage(this, rabbitEventArgs));
                        }
                    }

                    ModelCentralized.BasicAck(e.DeliveryTag, false);
                }
                catch (ThreadAbortException)
                {
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    break;
                }
            }
        }

        public void Dispose()
        {
            if (queuePublicThread != null)// && queuePublicThread.ThreadState != ThreadState.Stopped)
            {
                try
                {
                    queuePublicThread.Abort();
                    queuePublicThread.Join();
                }
                catch { }
            }

            privateThreadCollection.ForEach(t => t.Dispose());

            if (Connection != null)
            {
                if (ModelCentralized != null)
                {
                    ModelCentralized.Dispose();
                }

                Connection.Close();
                Connection.Dispose();
            }
        }
    }

    public class PrivateMessageQueue : IDisposable
    {
        private Thread _queuePrivateThread;
        private IModel ModelInternal;
        private ConnectionFactory connectionFactory;
        private IConnection Connection;

        public event ReceivedPrivateMessage OnReceivedPrivateMessage;
        public delegate void ReceivedPrivateMessage(object sender, RabbitMQEventArgs e);

        public string QueueInternalName { private set; get; }

        public PrivateMessageQueue(IConnection connection, ConnectionFactory connectionF, string queuePrivateName =null)
        {
            Connection = connection;
            connectionFactory = connectionF;
            QueueInternalName = queuePrivateName;

            if (ModelInternal != null)
            {
                // Already created
                throw new ApplicationException("Private queue already open");
            }

            if (connectionFactory == null)
            {
                connectionFactory = new ConnectionFactory();
            }

            ModelInternal = Connection.CreateModel();
            var replyInternalQueue = ModelInternal.QueueDeclare(QueueInternalName ?? string.Empty, false, true, true, null);
            QueueInternalName = replyInternalQueue.QueueName;

            _queuePrivateThread = new Thread(new ThreadStart(WaitQueuePrivateRequest));
            _queuePrivateThread.Start();

        }
        private void WaitQueuePrivateRequest()
        {
            QueueingBasicConsumer consumer;
            string consumerTag;
            //lock (Connection)
            {
                // Pipelining of requests forbidden
                consumer = new QueueingBasicConsumer(ModelInternal);
                consumerTag = ModelInternal.BasicConsume(QueueInternalName, false, consumer);
            }

            while (true)
            {
                try
                {
                    RabbitMQ.Client.Events.BasicDeliverEventArgs e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    IBasicProperties props = e.BasicProperties;
                    string messageId = props.MessageId;
                    string queueSender = props.ReplyTo;
                    //if (props.Headers["MessageId"] != null) { messageId = RabbitHelper.GetStringFromBytes((byte[])props.Headers["MessageId"]); }
                    //if (props.Headers["QueueName"] != null) { queueSender = RabbitHelper.GetStringFromBytes((byte[])props.Headers["QueueName"]); }
                    byte[] body = e.Body;

                    if (OnReceivedPrivateMessage != null)
                    {
                        OnReceivedPrivateMessage(this, new RabbitMQEventArgs(messageId, queueSender, body));
                    }

                    ModelInternal.BasicAck(e.DeliveryTag, false);
                }
                catch (ThreadAbortException)
                {
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    break;
                }
            }
        }
        public void Dispose()
        {
            if (_queuePrivateThread!=null)
            {
                _queuePrivateThread.Abort();
                _queuePrivateThread.Join();
            }

            if (ModelInternal != null)
            {
                ModelInternal.Dispose();
            }
        }
    }
}
