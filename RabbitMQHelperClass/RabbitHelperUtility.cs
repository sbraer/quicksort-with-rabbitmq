﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQLibrary
{
    public partial class RabbitHelper
    {
        public static string GetRandomMessageId()
        {
            return Guid.NewGuid().ToString();
        }

        public static byte[] GetBytesFromString(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            return Encoding.Default.GetBytes(text);
        }

        public static string GetStringFromBytes(byte[] stream)
        {
            try
            {
                return Encoding.Default.GetString(stream);
            }
            catch
            {
                return null;
            }
        }

        public static byte[] ObjectToByteArray<T>(T obj) where T : class
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static T ByteArrayToObject<T>(byte[] arrBytes) where T : class
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                T obj = binForm.Deserialize(memStream) as T;
                return obj;
            }
        }
    }
}
