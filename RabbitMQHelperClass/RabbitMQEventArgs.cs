﻿using System;

namespace RabbitMQLibrary
{
    public class RabbitMQEventArgs : EventArgs
    {
        public byte[] MessageContent { private set; get; }
        public string MessageId { private set; get; }
        public string QueueName { private set; get; }
        public RabbitMQEventArgs(string messageId, string queueName, byte[] msg)
        {
            MessageId = messageId;
            MessageContent = msg;
            QueueName = queueName;
        }
    }
}
