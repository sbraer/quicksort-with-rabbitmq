﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example4A
{
    class Program
    {
        const string ExchangeName = "ExchangeExample4";
        const string RoutingKey = "RoutingExample4";
        const string QueueName = "QueueExample4";
        static void Main(string[] args)
        {
            var connectionFactory = new ConnectionFactory();
            connectionFactory.HostName = "localhost";
            using (var Connection = connectionFactory.CreateConnection())
            {
                var ModelCentralized = Connection.CreateModel();
                ModelCentralized.QueueDeclare(QueueName, false, false, true, null);
                ModelCentralized.ExchangeDeclare(ExchangeName, ExchangeType.Direct);
                ModelCentralized.QueueBind(QueueName, ExchangeName, RoutingKey);
                ModelCentralized.BasicQos(0, 1, false);

                QueueingBasicConsumer consumer = new QueueingBasicConsumer(ModelCentralized);
                string consumerTag = ModelCentralized.BasicConsume(QueueName, false, consumer);

                Console.WriteLine("Wait incoming message...");

                while (true)
                {
                    var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs)consumer.Queue.Dequeue();
                    string content = Encoding.Default.GetString(e.Body);
                    Console.WriteLine("> {0}", content);
                    ModelCentralized.BasicAck(e.DeliveryTag, false);
                }
            }
        }
    }
}
