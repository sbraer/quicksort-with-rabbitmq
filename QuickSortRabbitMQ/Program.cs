﻿using RabbitMQLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickSortRabbitMQ
{
    class Program
    {
        static RabbitHelper rh = null;
        static string queueName = "QueueSort";
        static string exchangeName = "ExchangeSort";
        static string routingKey = "SortArray";
        static string privateQueueName, privateQueueNameResult;
        private static List<RequestSubmited> requestCache = new List<RequestSubmited>();
        private static Dictionary<string, List<int>> replyCache = new Dictionary<string, List<int>>();
        static void Main(string[] args)
        {
            Random rnd = new Random();
            var arrayToSort = new List<int>();

            for (int i = 0; i < 100; i++)
            {
                arrayToSort.Add(rnd.Next(1, 100));
            }

            if (arrayToSort.Count < 1000)
            {
                // show before sort
                Console.Write("Actual order: ");
                arrayToSort.ForEach(t => Console.Write(" {0}", t));
                Console.WriteLine();
            }

            using (rh = new RabbitHelper("localhost"))
            {
                rh.AddPublicQueue(queueName, exchangeName, routingKey, false);
                var privateQueueThread = rh.AddPrivateQueue();// "QueueRicorsiva");
                privateQueueName = privateQueueThread.QueueInternalName;

                var privateQueueResultThread = rh.AddPrivateQueue();// "QueueFinale");
                privateQueueNameResult = privateQueueResultThread.QueueInternalName;

                string messageId = RabbitHelper.GetRandomMessageId();

                rh.OnReceivedPublicMessage += Rh_OnReceivedPublicMessage;
                privateQueueThread.OnReceivedPrivateMessage += Rh_OnReceivedPrivateMessage;
                privateQueueResultThread.OnReceivedPrivateMessage += Rh_OnReceivedPrivateMessageResult;

                Console.ReadLine();
                Console.WriteLine("Start order...");
                var msgRaw = RabbitHelper.ObjectToByteArray<List<int>>(arrayToSort);
                rh.SendMessageToPublicQueue(msgRaw, messageId, privateQueueNameResult);

                Console.ReadLine();
            }

        }

        private static void Rh_OnReceivedPublicMessage(object sender, RabbitMQEventArgs e)
        {
            string messageId = e.MessageId;
            string queueFrom = e.QueueName;
            var toOrder = RabbitHelper.ByteArrayToObject<List<int>>(e.MessageContent);

            Console.Write(" " + toOrder.Count.ToString());

            if (toOrder.Count <= 1)
            {
                var msgRaw = RabbitHelper.ObjectToByteArray<List<int>>(toOrder);
                rh.SendMessageToPrivateQueue(msgRaw, messageId, queueFrom);
                return;
            }

            // else
            int pivot_index = toOrder.Count / 2;
            int pivot_value = toOrder[pivot_index];

            var less = new List<int>();
            var greater = new List<int>();

            for (int i = 0; i < toOrder.Count; i++)
            {
                if (i == pivot_index)
                {
                    continue;
                }

                if (toOrder[i] < pivot_value)
                {
                    less.Add(toOrder[i]);
                }
                else
                {
                    greater.Add(toOrder[i]);
                }
            }

            var rs = new RequestSubmited
            {
                MessageParentId = messageId,
                MessageId = RabbitHelper.GetRandomMessageId(),
                QueueFrom = queueFrom,
                PivotValue = pivot_value
            };
            lock (requestCache)
            {
                requestCache.Add(rs);
            }

            var msgRaw1 = RabbitHelper.ObjectToByteArray<List<int>>(less);
            rh.SendMessageToPublicQueue(msgRaw1, rs.MessageId, privateQueueName);
            var msgRaw2 = RabbitHelper.ObjectToByteArray<List<int>>(greater);
            rh.SendMessageToPublicQueue(msgRaw2, rs.MessageId, privateQueueName);
        }
        private static void Rh_OnReceivedPrivateMessageResult(object sender, RabbitMQEventArgs e)
        {
            //throw new NotImplementedException();
            Console.WriteLine("\r\n\r\nIo ho finito:");
            var sorted = RabbitHelper.ByteArrayToObject<List<int>>(e.MessageContent);
            foreach (var item in sorted)
            {
                Console.Write("{0}, ", item);
            }

            Console.WriteLine();
        }

        private static void Rh_OnReceivedPrivateMessage(object sender, RabbitMQEventArgs e)
        {
            string messageId = e.MessageId;
            string queueFrom = e.QueueName;
            var sorted1 = RabbitHelper.ByteArrayToObject<List<int>>(e.MessageContent);
            lock (string.Intern(messageId))
            {
                if (!replyCache.ContainsKey(messageId))
                {
                    replyCache[messageId] = sorted1;
                    return;
                }
            }

            var sorted2 = replyCache[messageId];
            replyCache.Remove(messageId);

            RequestSubmited actualRequest;

            lock (requestCache)
            {
                actualRequest = requestCache.Where(t => t.MessageId == messageId).FirstOrDefault();
                requestCache.Remove(actualRequest);
            }

            var pivotValue = actualRequest.PivotValue;

            var messageParentId = actualRequest.MessageParentId;
            string queueParent = actualRequest.QueueFrom;

            var result = new List<int>();
            if (sorted1.Count == 0 && sorted2.Count > 0)
            {
                if (sorted2.Min() < pivotValue)
                {
                    result.AddRange(sorted2);
                    result.Add(pivotValue);
                }
                else
                {
                    result.Add(pivotValue);
                    result.AddRange(sorted2);
                }
            }
            else if (sorted2.Count == 0 && sorted1.Count > 0)
            {
                if (sorted1.Max() > pivotValue)
                {
                    result.Add(pivotValue);
                    result.AddRange(sorted1);
                }
                else
                {
                    result.AddRange(sorted1);
                    result.Add(pivotValue);
                }
            }
            else if (sorted2.Count == 0 && sorted1.Count == 0)
            {
                result.Add(pivotValue);
            }
            else if (sorted1.Min() < sorted2.Min())
            {
                result.AddRange(sorted1);
                result.Add(pivotValue);
                result.AddRange(sorted2);
            }
            else
            {
                result.AddRange(sorted2);
                result.Add(actualRequest.PivotValue);
                result.AddRange(sorted1);
            }

            if (!string.IsNullOrEmpty(messageParentId))
            {
                var msgRaw = RabbitHelper.ObjectToByteArray<List<int>>(result);
                rh.SendMessageToPrivateQueue(msgRaw, messageParentId, queueParent);
            }
        }
    }

    public class RequestSubmited
    {
        public string MessageId { get; set; }
        public string MessageParentId { get; set; }
        public string QueueFrom { get; set; }
        public int PivotValue { get; set; }
    }

}
